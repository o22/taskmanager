package com.o22.taskmanager.view;

import com.o22.taskmanager.controller.Controller;
import com.o22.taskmanager.model.Task;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;


import java.util.Calendar;

import static junit.framework.Assert.*;

/**
 * Created by Александр on 19.11.2015.
 */
public class testController {
    @Test
    public void ControllerTest(){
        ApplicationContext genericXmlApplicationContext;
        genericXmlApplicationContext = new GenericXmlApplicationContext("app-context.xml");

        Controller c= (Controller) genericXmlApplicationContext.getBean("controller");

        Task t = new Task("task2","descrTask1", Calendar.getInstance(),"contactsTask1");
        t.getContacts();
        assertEquals("contactsTask1",t.getContacts());
        Calendar cal=Calendar.getInstance();
        cal.set(Calendar.MINUTE,59);
        c.getJournal();
        c.addTask("task1", cal, "contactsTask1", "descrTask1");
        assertEquals(c.getJournal().size(),1);
        for(Task task:c.getJournal()){
            c.deleteTask(task);
        }


        assertEquals(c.getJournal().size(), 0);
    }
}
