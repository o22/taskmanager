package com.o22.taskmanager.view.components;

import com.o22.taskmanager.model.Task;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.util.*;

public class TaskTableModel extends AbstractTableModel {

    private List<Task> tasks;

    public TaskTableModel(Set<Task> taskSet) {

        tasks = new ArrayList<Task>();

        updateModel(taskSet);
    }


    public int getRowCount() {
        return tasks.size();
    }

    public int getColumnCount() {
        return 4;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0: return "Name";
            case 1: return "Description";
            case 2: return "Date";
            case 3: return "Contacts";
        }
        return "";
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Task task = tasks.get(rowIndex);
        switch (columnIndex) {
            case -1: return task;
            case 0: return task.getName();
            case 1: return task.getDescription();
            case 2: return task.getOrgDate();
            case 3: return task.getContacts();
        }
        return "";
    }

    public void updateModel(Set<Task> taskSet) {
        tasks.clear();
        for (Task task : taskSet) tasks.add(task);
        fireTableDataChanged();
    }
}
