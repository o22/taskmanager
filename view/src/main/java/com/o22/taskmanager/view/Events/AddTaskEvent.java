package com.o22.taskmanager.view.events;

import com.o22.taskmanager.model.Task;

/**
 * Created by Александр on 10.12.2015.
 */
public class AddTaskEvent  extends ClientEvent {
    private Task task;

    public Task getTask() {
        return task;
    }

    public AddTaskEvent(Task task) {
        this.task=task;

    }
}
