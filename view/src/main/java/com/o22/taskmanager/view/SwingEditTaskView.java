package com.o22.taskmanager.view;


import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.o22.taskmanager.controller.IController;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.events.AddTaskEvent;
import com.o22.taskmanager.view.events.RemoveTaskEvent;
import com.o22.taskmanager.view.events.ShowEditViewEvent;
import com.o22.taskmanager.view.exception.InvalidDataException;
import net.sourceforge.jdatepicker.DateModel;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SwingEditTaskView extends JDialog {

    private JTextField nameField;
    private JTextField descField;
    private JDatePickerImpl datePicker;
    private JTextField timeField;
    private JTextField contactsField;
    private JSpinner hourPicker;
    private JSpinner minPicker;
    private IController controller;
    private Task task;
    private InputStream in;
    private DataOutputStream out;
    private ObjectOutputStream oos;
    private EventBus eb;


    public SwingEditTaskView(IController controller, Task task) {
        this.controller = controller;
        this.task = task;


    }

    public SwingEditTaskView(EventBus eventbus, Task task) {
        this.task = task;
        this.eb = eventbus;


    }

    @Subscribe
    public void showEditView(ShowEditViewEvent s) {

        this.task = null;

        this.task = s.getTask();
        this.display();

    }

    public void display() {
        Calendar c = new GregorianCalendar();


        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        if (task == null)
            setTitle("Creating a task");
        else
            setTitle("Changing a task");
        setSize(280, 300);
        setResizable(false);

        JLabel nameLabel = new JLabel();
        nameLabel.setText("Enter name");

        nameField = new JTextField();

        JLabel descLabel = new JLabel();
        descLabel.setText("Enter description");

        descField = new JTextField();

        JLabel dateLabel = new JLabel();
        dateLabel.setText("Select date");

        UtilDateModel model = new UtilDateModel();
        model.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        model.setSelected(true);
        JDatePanelImpl datePanel = new JDatePanelImpl(model);
        datePicker = new JDatePickerImpl(datePanel);

        JLabel timeLabel = new JLabel();
        timeLabel.setText("Enter time (hh:mm)");

        timeField = new JTextField();

        JLabel contactsLabel = new JLabel();
        contactsLabel.setText("Enter contacts");

        contactsField = new JTextField();

        final JButton button = new JButton();
        if (task == null)
            button.setText("Add task");
        else
            button.setText("Change task");
        button.addActionListener(new ButtonActionListener());
        SpinnerModel hoursModel =
                new SpinnerNumberModel(c.get(Calendar.HOUR_OF_DAY),
                        0, //min
                        24, //max
                        1);//step
        SpinnerModel minModel =
                new SpinnerNumberModel(c.get(Calendar.MINUTE),
                        0, //min
                        59, //max
                        1);//step

        hourPicker = new JSpinner(hoursModel);

        minPicker = new JSpinner(minModel);
        hourPicker.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                timeField.setText("Value : "
                        + ((JSpinner) e.getSource()).getValue());
            }
        });
        hourPicker.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                timeField.setText("Value : "
                        + ((JSpinner) e.getSource()).getValue());
            }
        });
        if (task != null) {
            nameField.setText(task.getName());
            descField.setText(task.getDescription());
            contactsField.setText(task.getContacts());
            hourPicker.setValue(task.getDate().get(Calendar.HOUR_OF_DAY));
            minPicker.setValue(task.getDate().get(Calendar.MINUTE));
            datePicker.getModel().setYear(task.getDate().get(Calendar.YEAR));
            datePicker.getModel().setMonth(task.getDate().get(Calendar.MONTH));
            datePicker.getModel().setDay(task.getDate().get(Calendar.DAY_OF_MONTH));

        }


        JPanel panel = new JPanel(new GridBagLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.add(nameLabel, new GridBagConstraints(0, 0, 1, 1, 0, GridBagConstraints.NONE, GridBagConstraints.NORTHWEST, 0, new Insets(10, 10, 10, 10), 0, 0));
        panel.add(nameField, new GridBagConstraints(1, 0, 1, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, 0, new Insets(10, 10, 10, 10), 150, 0));
        panel.add(descLabel, new GridBagConstraints(0, 1, 1, 1, 0, GridBagConstraints.NONE, GridBagConstraints.NORTHWEST, 0, new Insets(10, 10, 10, 10), 0, 0));
        panel.add(descField, new GridBagConstraints(1, 1, 1, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, 0, new Insets(10, 10, 10, 10), 150, 0));
        panel.add(dateLabel, new GridBagConstraints(0, 2, 1, 1, 0, GridBagConstraints.NONE, GridBagConstraints.NORTHWEST, 0, new Insets(10, 10, 10, 10), 0, 0));
        panel.add(datePicker, new GridBagConstraints(1, 2, 1, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, 0, new Insets(10, 10, 10, 10), 150, 0));
        panel.add(timeLabel, new GridBagConstraints(0, 3, 1, 1, 0, GridBagConstraints.NONE, GridBagConstraints.NORTHWEST, 0, new Insets(10, 10, 10, 10), 0, 0));
        //panel.add(timeField, new GridBagConstraints(1, 3, 1, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, 0, new Insets(10, 10, 10, 10), 150, 0));
        panel.add(minPicker, new GridBagConstraints(1, 3, 2, 1, 1, GridBagConstraints.NONE, GridBagConstraints.EAST, 0, new Insets(10, 10, 10, 10), 0, 0));
        panel.add(hourPicker, new GridBagConstraints(1, 3, 2, 1, 1, GridBagConstraints.NONE, GridBagConstraints.WEST, 0, new Insets(10, 10, 10, 10), 0, 0));

        panel.add(contactsLabel, new GridBagConstraints(0, 4, 1, 1, 0, GridBagConstraints.NONE, GridBagConstraints.NORTHWEST, 0, new Insets(10, 10, 10, 10), 0, 0));
        panel.add(contactsField, new GridBagConstraints(1, 4, 1, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, 0, new Insets(10, 10, 10, 10), 150, 0));
        panel.add(button, new GridBagConstraints(0, 5, 2, 1, 0, GridBagConstraints.NONE, GridBagConstraints.NORTHWEST, 0, new Insets(10, 10, 10, 10), 0, 0));

        add(panel);

        setVisible(true);

    }

    private void clearAll() {
        this.task = null;
       /* Calendar c = new GregorianCalendar();
        nameField.setText(null);
        descField.setText(null);

        hourPicker.setValue(c.get(Calendar.HOUR_OF_DAY));
        minPicker.setValue(c.get(Calendar.MINUTE));
        contactsField.setText(null);*/


    }

    private boolean validateData() throws InvalidDataException {
        if (nameField.getText().isEmpty())
            throw new InvalidDataException();
        if (descField.getText().isEmpty())
            throw new InvalidDataException();
        return true;
    }

    private class ButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                if (validateData()) {
                    String name = nameField.getText();
                    String desc = descField.getText();
                    DateModel dateModel = datePicker.getModel();
                    Calendar dateCalendar = new GregorianCalendar(dateModel.getYear(),dateModel.getMonth(),dateModel.getDay(),(Integer)hourPicker.getValue(),(Integer) minPicker.getValue());
                   // LocalDate date = LocalDate.of(dateModel.getYear(), dateModel.getMonth(), dateModel.getDay());
                    //LocalTime time = LocalTime.of((Integer)hourPicker.getValue(),(Integer)minPicker.getValue(),0,0);
                    String contacts = contactsField.getText();
                    if (controller != null)
                        controller.addTask(name,  dateCalendar,desc, contacts);
                    else {
                        Calendar calendar =dateCalendar;

                        //addSerial("Add", new Task(name, desc, calendar, contacts));
                        // addBinary("Add " + name + " " + desc + " " + calendar.toString() + " " + contacts);
                        // addOneByOne(name, desc, calendar.toString(), contacts);
                        if (task == null) {
                            Task t = new Task(name, desc, calendar, contacts);
                            eb.post(new AddTaskEvent(t));
                        } else {
                            Task t2 = new Task(name, desc, calendar, contacts);
                            eb.post(new RemoveTaskEvent(task));
                            eb.post(new AddTaskEvent(t2));
                        }


                    }

                    clearAll();
                    setVisible(false);
                    dispose();
                }
            } catch (InvalidDataException exception) {
                SwingAlertBox alertBox = new SwingAlertBox();
                alertBox.display("Invalid data", "Enter valid data");
            } catch (DateTimeParseException exception) {
                SwingAlertBox alertBox = new SwingAlertBox();
                alertBox.display("Invalid time", "Enter valid time");
            }
        }
    }

}
