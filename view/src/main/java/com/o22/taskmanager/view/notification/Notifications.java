package com.o22.taskmanager.view.notification;


import com.o22.taskmanager.controller.INotification;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.notification.factory.INotificationFactory;
import com.o22.taskmanager.view.notification.factory.SwingINotificationFactory;

public class Notifications {

    private static INotificationFactory factory = new SwingINotificationFactory();

    public static void setFactory(INotificationFactory newFactory) {
        factory = newFactory;
    }

    public static INotification createNotification(Task task) {
        return factory.createNotification(task);
    }


}
