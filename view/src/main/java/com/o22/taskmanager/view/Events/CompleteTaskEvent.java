package com.o22.taskmanager.view.events;

import com.o22.taskmanager.model.Task;

/**
 * Created by Александр on 13.12.2015.
 */
public class CompleteTaskEvent {
    private Task task;
    public CompleteTaskEvent(Task task) {
        this.task=task;

    }
    public Task getTask(){
        return task;
    }
}
