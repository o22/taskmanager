package com.o22.taskmanager.view.notification.swing;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.events.*;
import com.o22.taskmanager.view.components.NotificationPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SwingNewNotifications extends JDialog {

    private int current;
    private EventBus ebus;
    private List<NotificationPanel> notifications;
    boolean d=false,n=false;
    JButton completeButton = new JButton();
    JPanel buttonsPanel = new JPanel();
    JButton nextButton = new JButton();
    JButton prevButton = new JButton();
    JButton postponeButton = new JButton();
    final JPanel mainPanel = new JPanel(new BorderLayout(10, 10));
    final JLabel slideLabel = new JLabel();
    public SwingNewNotifications(EventBus ebus){
        this.ebus = ebus;
        current = 0;
        notifications = new ArrayList<NotificationPanel>();

    }
    @Subscribe
    public void onAddTask(EventNotification event){
        notifications.add(new NotificationPanel(event.getTask()));

        //current++;
        if(!d) {
            display();
            d=true;
        }
        slideLabel.setText("1/" + notifications.size());
        this.revalidate();

    }


    public void display() {

        setTitle("New Notification!");
        //setModal(true);
        setLocationRelativeTo(null);
        setSize(500, 400);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                notifications.clear();
            }
        });


        prevButton.setText("PREV");

        completeButton.setText("Complete");

        postponeButton.setText("Postpone");


        nextButton.setText("NEXT");


        slideLabel.setText("1/" + notifications.size());


        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));

        buttonsPanel.add(Box.createHorizontalGlue());

        buttonsPanel.add(prevButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsPanel.add(slideLabel);
        buttonsPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsPanel.add(nextButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsPanel.add(completeButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsPanel.add(postponeButton);
        buttonsPanel.add(Box.createHorizontalGlue());



        mainPanel.add(notifications.get(0), BorderLayout.CENTER);
        mainPanel.add(buttonsPanel, BorderLayout.SOUTH);

        add(mainPanel);

        prevButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (current > 0) {
                    mainPanel.remove(notifications.get(current));
                    mainPanel.add(notifications.get(--current), BorderLayout.CENTER);
                    mainPanel.repaint();
                    slideLabel.setText((current + 1) + "/" + notifications.size());
                }
            }
        });

        nextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (current < notifications.size() - 1) {
                    mainPanel.remove(notifications.get(current));
                    mainPanel.add(notifications.get(++current), BorderLayout.CENTER);
                    mainPanel.repaint();
                    slideLabel.setText((current + 1) + "/" + notifications.size());
                }
            }
        });
        completeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                d=false;
                ebus.post(new RemoveTaskEvent(notifications.get(current).getTask()));
                mainPanel.remove(notifications.get(current));
                notifications.remove(notifications.get(current));
                current = 0;
                if (notifications.size() == 0) {
                    setVisible(false);
                } else {
                    mainPanel.add(notifications.get(current), BorderLayout.CENTER);
                    mainPanel.repaint();
                    slideLabel.setText((current + 1) + "/" + notifications.size());
                }

            }
        });
        postponeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                d=false;
                ebus.post(new PostPoneTaskEvent(0, 5, notifications.get(current).getTask()));
                mainPanel.remove(notifications.get(current));
                notifications.remove(notifications.get(current));
                current = 0;
                if (notifications.size() == 0) {
                    setVisible(false);
                } else {
                    mainPanel.add(notifications.get(current), BorderLayout.CENTER);
                    mainPanel.repaint();
                    slideLabel.setText((current + 1) + "/" + notifications.size());
                }
            }
        });

        setVisible(true);

    }
}

