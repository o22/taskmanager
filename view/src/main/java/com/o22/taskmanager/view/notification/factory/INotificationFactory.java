package com.o22.taskmanager.view.notification.factory;

import com.o22.taskmanager.controller.INotification;
import com.o22.taskmanager.model.Task;

public interface INotificationFactory {

    INotification createNotification(Task task);
}
