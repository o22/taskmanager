package com.o22.taskmanager.view;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.o22.taskmanager.model.Constants;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.events.*;
import com.o22.taskmanager.view.notification.swing.SwingNewNotifications;
import com.o22.taskmanager.view.notification.swing.SwingNotification;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.Socket;
import java.util.*;

@Component
public class StartClient {
    private static final Logger LOG = Logger.getLogger(StartClient.class);
    Socket s = null;
    ObjectInputStream ois;
    ObjectOutputStream oos;
    DataInputStream in;
    DataOutputStream out;
    EventBus eventBus;
    ClientMainView cmw;
    Set<Task> m = new HashSet<Task>();
    SwingNewNotifications sn;
    SwingEditTaskView editTask;
    Set<Task> checkedTasks = new HashSet<Task>();

    public StartClient() {

        LOG.debug("Starting client...");
        eventBus = new EventBus();

        initStreams();


        Set<Task> j = new HashSet<Task>();
        j = getTasks();
        for (Task task : j) {
            clientStartTimer(task);
        }

        cmw = new ClientMainView(j, m, eventBus);
        editTask = new SwingEditTaskView(eventBus, null);

        sn = new SwingNewNotifications(eventBus);
        registerOnEbus();

        cmw.display();

    }

    public void registerOnEbus() {
        eventBus.register(cmw);
        eventBus.register(this);
        eventBus.register(sn);
        eventBus.register(editTask);
    }

    public void initStreams() {
        try {
            s = new Socket(Constants.LOCALHOST, Constants.PORT);
            out = new DataOutputStream(s.getOutputStream());

            in = new DataInputStream(s.getInputStream());

            oos = new ObjectOutputStream(s.getOutputStream());
            ois = new ObjectInputStream(s.getInputStream());
        } catch (IOException e) {
            LOG.debug("IOException in client", e);
        }


    }


    @Subscribe
    public void onExit(ExitEvent event) {
        try {
            writeCommand(Constants.EXIT);
            in.close();
            out.close();
            ois.close();
            oos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Subscribe
    public void onAddTask(AddTaskEvent event) throws IOException, ClassNotFoundException {
        writeCommand(Constants.ADD_TASK);
        Task test = event.getTask();
        writeObj(test);
        clientStartTimer(test);

        Set<Task> j = getTasks();
        eventBus.post(new UpdateEvent(j));

    }

    @Subscribe
    public void onRemoveTask(RemoveTaskEvent event) throws IOException, ClassNotFoundException {

        writeCommand(Constants.DELETE_TASK);
        writeObj(event.getTask());


        Set<Task> j = new HashSet<Task>();
        j = getTasks();
        eventBus.post(new UpdateEvent(j));

    }

    public void clientStartTimer(Task t) {
        try {


            ClientTaskTimer task = new ClientTaskTimer(t, eventBus);
            eventBus.register(task);
            Timer timer = new Timer();
            if(t.getDate().before(Calendar.getInstance()))
                throw new IllegalArgumentException();
            timer.schedule(task,t.getDate().getTime());


            LOG.debug("Start timer for task" + t.getName());
            //log.debug("Start timer for task " + t.getName());
        } catch (IllegalArgumentException e) {
            //log.debug("Task " + t.getName() + " are outdated", e);
            m.add(t);
            LOG.debug("Add Task to missedtask");
            //deleteTask(t);
        }

    }


    public void writeObj(Object obj) {
        try {
            oos.reset();
            LOG.debug("Write object to outputstream");
            oos.writeObject(obj);
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeCommand(String str) {
        try {
            LOG.debug("Write command to outputstream");
            out.writeUTF(str);
            out.flush();
        } catch (IOException e) {
            LOG.debug(e);
        }

    }

    @Subscribe
    public void onPostPone(PostPoneTaskEvent event) throws IOException, ClassNotFoundException {
        Calendar today = new GregorianCalendar();
        onRemoveTask(new RemoveTaskEvent(event.getTask()));
        Set<Task> j = new HashSet<Task>();
        j = getTasks();
        Task t = event.getTask();
        t.setOrgDate(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), today.get(Calendar.HOUR) + event.getHour(), today.get(Calendar.MINUTE) + event.getMin(), 0);
        Task new_t = t;
        this.onAddTask(new AddTaskEvent(new_t));

        clientStartTimer(new_t);
        j = new HashSet<Task>();
        j = getTasks();
        eventBus.post(new UpdateEvent(j));
    }

    public Set<Task> getTasks() {
        try {
            writeCommand(Constants.GET_JOURNAL);
            LOG.debug("Getting tasks from inputstream");
            return (Set<Task>) ois.readObject();
        } catch (IOException e) {
            LOG.debug(e);
        } catch (ClassNotFoundException e) {
            LOG.debug(e);
        }
        return null;
    }


}
