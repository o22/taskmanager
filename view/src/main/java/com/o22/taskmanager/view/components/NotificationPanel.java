package com.o22.taskmanager.view.components;

import com.o22.taskmanager.model.Task;

import javax.swing.*;
import java.awt.*;

public class NotificationPanel extends JPanel {
private Task task;
    public NotificationPanel(Task task) {

        super(new BorderLayout(10, 10));
        this.task=task;
        setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JLabel taskName = new JLabel();
        taskName.setText("Task name: " + task.getName());
        taskName.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel taskDescription = new JLabel();
        taskDescription.setText("Task description: " + task.getDescription());
        taskDescription.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel taskDate = new JLabel();
        taskDate.setText("Task date: " + task.getOrgDate());
        taskDate.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel taskContacts = new JLabel();
        taskContacts.setText("Task contacts: " + task.getContacts());
        taskContacts.setHorizontalAlignment(SwingConstants.CENTER);

        JPanel messagePanel = new JPanel();
        messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.Y_AXIS));
        messagePanel.add(taskName);
        messagePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        messagePanel.add(taskDescription);
        messagePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        messagePanel.add(taskDate);
        messagePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        messagePanel.add(taskContacts);

        add(messagePanel, BorderLayout.CENTER);
    }
    public Task getTask(){
        return task;
    }

}
