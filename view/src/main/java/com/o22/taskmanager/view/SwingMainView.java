package com.o22.taskmanager.view;


import com.o22.taskmanager.controller.IController;
import com.o22.taskmanager.controller.Observable;
import com.o22.taskmanager.controller.Observer;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.components.TaskTableModel;
import com.o22.taskmanager.view.notification.swing.SwingMissedNotifications;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.util.Set;

public class SwingMainView extends JFrame implements Observer {

    private IController controller;

    private JTable taskView;
    private TaskTableModel taskModel;
    private String iconSTR="ico.png";



    public SwingMainView(IController controller) {
        super("Task manager");
        this.controller = controller;
        Observable observable = (Observable) controller;
        observable.addObserver(this);
    }

    public void display() {

        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getInsets().set(10, 10, 10, 10);

        JPanel mainScene = new JPanel(new BorderLayout());
        mainScene.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        add(mainScene);
        /*Start Lebarto*/
        JButton changeButton = new JButton();
        changeButton.setText("Change task");
        changeButton.addActionListener(new ChangeButtonListener());
        //Spinner Model for hours







        /*       End*/
        JButton addButton = new JButton();
        addButton.setText("Add task");
        addButton.addActionListener(new AddButtonActionListener());

        JButton removeButton = new JButton();
        removeButton.setText("Remove task");
        removeButton.addActionListener(new RemoveButtonActionListener());

        taskModel = new TaskTableModel(controller.getJournal());
        taskView = new JTable(taskModel);
        taskView.setAutoCreateRowSorter(true);
        JScrollPane scrollPane = new JScrollPane(taskView);

        Panel buttonsPanel = new Panel(new FlowLayout());
        buttonsPanel.add(addButton);
        buttonsPanel.add(removeButton);
        buttonsPanel.add(changeButton);


        mainScene.add(scrollPane, BorderLayout.CENTER);
        mainScene.add(buttonsPanel, BorderLayout.SOUTH);

        if (!controller.getMissedTasks().isEmpty()) displayMissedNotifications(controller.getMissedTasks());

        setVisible(true);
        pack();
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowIconified(WindowEvent e) {
                SwingMainView.this.setVisible(false);
                setTrayIcon();

            }

        });
    }

    public void updateUI() {
        taskModel.updateModel(controller.getJournal());
    }

    private void displayMissedNotifications(Set<Task> taskSet) {
        SwingMissedNotifications notifications = new SwingMissedNotifications(taskSet);
        notifications.display();
    }

    public SwingMainView getInstance() {
        return this;
    }

    public void update(Object obj) {
        updateUI();
    }

    private class AddButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            SwingEditTaskView editTaskView = new SwingEditTaskView(controller,null);
            editTaskView.display();
        }
    }

    private class RemoveButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            SwingAlertBox alert = new SwingAlertBox();
            if (taskView.getSelectedRow() == -1)
                alert.display("Error", "Select the task to remove.");
            else {
                SwingConfirmBox confirmBox = new SwingConfirmBox();
                confirmBox.display("Confirm", "Do you want to remove this task?");
                if (confirmBox.getAnswer()) {
                    int row = taskView.getSelectedRow();
                    Task task = (Task) taskView.getModel().getValueAt(taskView.convertRowIndexToView(row), -1);
                    controller.deleteTask(task);
                }
            }
        }
    }

    private class ChangeButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            SwingAlertBox alert = new SwingAlertBox();
            if (taskView.getSelectedRow() == -1)
                alert.display("Error", "Select the task to remove.");
            else {
                int row = taskView.getSelectedRow();
                Task task = (Task) taskView.getModel().getValueAt(taskView.convertRowIndexToView(row), -1);
                SwingEditTaskView editTaskView = new SwingEditTaskView(controller,task);
                editTaskView.display();
            }
        }
    }
    private void setTrayIcon(){
        final PopupMenu trayMenu=new PopupMenu();
        MenuItem exit = new MenuItem("Exit");
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        MenuItem open = new MenuItem("Open");
        open.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                SwingMainView.this.setVisible(true);
                SwingMainView.this.setExtendedState(JFrame.NORMAL);
            }
        });
        trayMenu.add(exit);
        trayMenu.add(open);

        Image icon = Toolkit.getDefaultToolkit().getImage(iconSTR);
        final TrayIcon trayicon = new TrayIcon(icon,"TaskManager",trayMenu);
        trayicon.setImageAutoSize(true);
        trayicon.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked (MouseEvent e){
                if ( SwingUtilities.isRightMouseButton( e )){
                    trayicon.setPopupMenu(trayMenu);
                }
                else if(SwingUtilities.isLeftMouseButton( e )){
                    SwingMainView.this.setVisible(true);
                    SwingMainView.this.setExtendedState(JFrame.NORMAL);
                }
            }
        });
        SystemTray sysTray=SystemTray.getSystemTray();
        try {
            sysTray.add(trayicon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
        trayicon.displayMessage("TaskManager", "Now in system tray",
                TrayIcon.MessageType.INFO);


    }
}
