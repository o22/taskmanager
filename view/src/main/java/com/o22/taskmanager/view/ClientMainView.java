package com.o22.taskmanager.view;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.o22.taskmanager.controller.IController;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.events.ExitEvent;
import com.o22.taskmanager.view.events.RemoveTaskEvent;
import com.o22.taskmanager.view.events.ShowEditViewEvent;
import com.o22.taskmanager.view.events.UpdateEvent;
import com.o22.taskmanager.view.components.TaskTableModel;
import com.o22.taskmanager.view.notification.swing.SwingMissedNotifications;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Set;

/**
 * Created by Александр on 05.12.2015.
 */
public class ClientMainView extends JFrame {

    private JTable taskView;
    private TaskTableModel taskModel;
    private String iconSTR = "ico.png";
    private IController controller;
    private EventBus ebus;
    private Set<Task> tasks;
    private Set<Task> missedTasks;
    private static final Logger LOG = Logger.getLogger(ClientMainView.class);

    public ClientMainView(Set<Task> tasks, Set<Task> missed, EventBus eBus) {
        super("Task manager");
        this.tasks = tasks;
        this.ebus = eBus;
        this.missedTasks = missed;
    }

    public void display() {
        LOG.debug("Display ClientMainView");
        setResizable(false);
        setLocationRelativeTo(null);
        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                ebus.post(new ExitEvent());
                e.getWindow().dispose();
            }
        });
        //setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getInsets().set(10, 10, 10, 10);

        JPanel mainScene = new JPanel(new BorderLayout());
        mainScene.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        add(mainScene);
        JButton changeButton = new JButton();
        changeButton.setText("Change task");
        changeButton.addActionListener(new ChangeButtonListener());

        JButton addButton = new JButton();
        addButton.setText("Add task");
        addButton.addActionListener(new AddButtonActionListener());

        JButton removeButton = new JButton();
        removeButton.setText("Remove task");
        removeButton.addActionListener(new RemoveButtonActionListener());


        taskModel = new TaskTableModel(tasks);
        taskView = new JTable(taskModel);
        taskView.setAutoCreateRowSorter(true);
        JScrollPane scrollPane = new JScrollPane(taskView);

        Panel buttonsPanel = new Panel(new FlowLayout());
        buttonsPanel.add(addButton);
        buttonsPanel.add(removeButton);
        buttonsPanel.add(changeButton);


        mainScene.add(scrollPane, BorderLayout.CENTER);
        mainScene.add(buttonsPanel, BorderLayout.SOUTH);
        //Set<Task> missedTask=(Set<Task>)getByCommand("getMissedTasks");
        if (missedTasks != null&&missedTasks.size()!=0) {
            displayMissedNotifications(missedTasks);
        }

        setVisible(true);
        pack();
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowIconified(WindowEvent e) {
                ClientMainView.this.setVisible(false);
                setTrayIcon();

            }

        });
    }
    @Subscribe
    public void updateUI(UpdateEvent event) {
        LOG.debug("Update UI");
        Set<Task> journal = event.getTasks();
        taskModel.updateModel(journal);
        taskView.updateUI();

    }

    private void displayMissedNotifications(Set<Task> taskSet) {
        LOG.debug("Display MissedNotifications");
        SwingMissedNotifications notifications = new SwingMissedNotifications(taskSet,ebus);
        notifications.display();

    }

    public ClientMainView getInstance() {
        return this;
    }

    private class AddButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            SwingEditTaskView setw = new SwingEditTaskView(ebus, null);
            setw.display();
            //ebus.post(new ShowEditViewEvent(null));
            //SwingEditTaskView editTaskView = new SwingEditTaskView(oos,out,null);
            //editTaskView.display(controller,null);
        }
    }

    private class RemoveButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            SwingAlertBox alert = new SwingAlertBox();
            if (taskView.getSelectedRow() == -1)
                alert.display("Error", "Select the task to remove.");
            else {
                SwingConfirmBox confirmBox = new SwingConfirmBox();
                confirmBox.display("Confirm", "Do you want to remove this task?");
                if (confirmBox.getAnswer()) {
                    int row = taskView.getSelectedRow();
                    Task task = (Task) taskView.getModel().getValueAt(taskView.convertRowIndexToView(row), -1);
                    ebus.post(new RemoveTaskEvent(task));
                    //controller.deleteTask(task);
                }
            }
        }
    }

    private class ChangeButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            SwingAlertBox alert = new SwingAlertBox();
            if (taskView.getSelectedRow() == -1)
                alert.display("Error", "Select the task to change.");
            else {
                int row = taskView.getSelectedRow();
                Task task = (Task) taskView.getModel().getValueAt(taskView.convertRowIndexToView(row), -1);

                SwingEditTaskView setw = new SwingEditTaskView(ebus, task);
                setw.display();
                //SwingEditTaskView editTaskView = new SwingEditTaskView(oos,out,task);
                //editTaskView.display(controller,task);
            }
        }
    }

    private void setTrayIcon() {
        final SystemTray sysTray = SystemTray.getSystemTray();
        final PopupMenu trayMenu = new PopupMenu();
        MenuItem exit = new MenuItem("Exit");
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        MenuItem open = new MenuItem("Open");

        trayMenu.add(exit);


        Image icon = Toolkit.getDefaultToolkit().getImage(iconSTR);
        final TrayIcon trayicon = new TrayIcon(icon, "TaskManager", trayMenu);
        trayicon.setImageAutoSize(true);
        open.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ClientMainView.this.setVisible(true);
                ClientMainView.this.setExtendedState(JFrame.NORMAL);
                sysTray.remove(trayicon);
            }
        });
        trayMenu.add(open);
        trayicon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    trayicon.setPopupMenu(trayMenu);
                } else if (SwingUtilities.isLeftMouseButton(e)) {
                    ClientMainView.this.setVisible(true);
                    ClientMainView.this.setExtendedState(JFrame.NORMAL);
                    sysTray.remove(trayicon);
                }
            }
        });

        try {
            sysTray.add(trayicon);

        } catch (AWTException e) {
            e.printStackTrace();
        }
        trayicon.displayMessage("TaskManager", "Now in system tray",
                TrayIcon.MessageType.INFO);


    }


}