package com.o22.taskmanager.view.events;

import com.o22.taskmanager.model.Task;

/**
 * Created by Александр on 12.12.2015.
 */
public class EventNotification {
    private Task task;
    public EventNotification(Task task) {
        System.out.println("EventNotification");
        this.task=task;
    }

    public Task getTask() {
        return task;
    }
}
