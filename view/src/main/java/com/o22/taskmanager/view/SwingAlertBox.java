package com.o22.taskmanager.view;

import com.o22.taskmanager.controller.INotification;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SwingAlertBox extends JDialog {

    public void display(String title, String message) {

        setTitle(title);
        setLocationRelativeTo(null);
        setSize(300, 130);
        setResizable(false);

        JPanel panel = new JPanel(new BorderLayout(10, 10));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JLabel messageLabel = new JLabel();
        messageLabel.setText(message);
        messageLabel.setHorizontalAlignment(SwingConstants.CENTER);

        final JButton okButton = new JButton();
        okButton.setText("OK");
        okButton.addActionListener(new OkButtonActionListener());

        panel.add(messageLabel, BorderLayout.CENTER);
        panel.add(okButton, BorderLayout.SOUTH);
        add(panel);

        setVisible(true);
    }

    private class OkButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            dispose();
        }
    }
}
