package com.o22.taskmanager.view;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.events.EventNotification;
import com.o22.taskmanager.view.events.RemoveTaskEvent;
import org.apache.log4j.Logger;

import java.util.TimerTask;

/**
 * Created by Александр on 12.12.2015.
 */
public class ClientTaskTimer extends TimerTask {
    private static final Logger log = Logger.getLogger(ClientTaskTimer.class);
    private EventBus eventBus;
    private Task task;

    ClientTaskTimer(Task t,EventBus eventBus) {
        task = t;
        eventBus.register(this);
        this.eventBus =eventBus;

    }
    @Subscribe
    public void cancelTask(RemoveTaskEvent event){

        if(this.task.equals(event.getTask())){

            this.cancel();
            log.debug("TaskTimer deleted");
        }

    }
    @Override
    public void run() {
            eventBus.post(new EventNotification(task));
            //alertBox.display();

        }

        //c.deleteTask(task)
}
