package com.o22.taskmanager.view;

import com.o22.taskmanager.controller.Controller;
import com.o22.taskmanager.controller.IController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {

    public static void main(String[] args) throws Exception {
        ApplicationContext genericXmlApplicationContext;
        genericXmlApplicationContext = new GenericXmlApplicationContext("app-context.xml");

    }
}
