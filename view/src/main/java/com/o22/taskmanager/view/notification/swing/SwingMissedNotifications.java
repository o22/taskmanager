package com.o22.taskmanager.view.notification.swing;

import com.google.common.eventbus.EventBus;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.events.PostPoneTaskEvent;
import com.o22.taskmanager.view.events.RemoveTaskEvent;
import com.o22.taskmanager.view.components.NotificationPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SwingMissedNotifications extends JDialog {

    private int current;
    private EventBus ebus;
    private List<NotificationPanel> notifications;

    public SwingMissedNotifications(Set<Task> tasks, EventBus ebus) {
        this.ebus = ebus;
        current = 0;

        notifications = new ArrayList<NotificationPanel>();

        for (Task task : tasks) {
            notifications.add(new NotificationPanel(task));
        }
    }

    public SwingMissedNotifications(Set<Task> tasks) {
        this.ebus = ebus;
        current = 0;

        notifications = new ArrayList<NotificationPanel>();

        for (Task task : tasks) {
            notifications.add(new NotificationPanel(task));
        }
    }

    public void display() {

        setTitle("Missed notifications");
        setModal(true);
        setLocationRelativeTo(null);
        setSize(500, 400);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        JButton prevButton = new JButton();
        prevButton.setText("PREV");
        JButton completeButton = new JButton();
        completeButton.setText("Complete");
        JButton postponeButton = new JButton();
        postponeButton.setText("Postpone");

        JButton nextButton = new JButton();
        nextButton.setText("NEXT");

        final JLabel slideLabel = new JLabel();
        slideLabel.setText("1/" + notifications.size());

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));

        buttonsPanel.add(Box.createHorizontalGlue());

        buttonsPanel.add(prevButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsPanel.add(slideLabel);
        buttonsPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsPanel.add(nextButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsPanel.add(completeButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsPanel.add(postponeButton);
        buttonsPanel.add(Box.createHorizontalGlue());


        final JPanel mainPanel = new JPanel(new BorderLayout(10, 10));
        mainPanel.add(notifications.get(0), BorderLayout.CENTER);
        mainPanel.add(buttonsPanel, BorderLayout.SOUTH);

        add(mainPanel);

        prevButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (current > 0) {
                    mainPanel.remove(notifications.get(current));
                    mainPanel.add(notifications.get(--current), BorderLayout.CENTER);
                    mainPanel.repaint();
                    slideLabel.setText((current + 1) + "/" + notifications.size());
                }
            }
        });

        nextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (current < notifications.size() - 1) {
                    mainPanel.remove(notifications.get(current));
                    mainPanel.add(notifications.get(++current), BorderLayout.CENTER);
                    mainPanel.repaint();
                    slideLabel.setText((current + 1) + "/" + notifications.size());
                }
            }
        });
        completeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ebus.post(new RemoveTaskEvent(notifications.get(current).getTask()));
                mainPanel.remove(notifications.get(current));
                notifications.remove(notifications.get(current));
                current = 0;
                if (notifications.size() == 0) {
                    dispose();
                } else {
                    mainPanel.add(notifications.get(current), BorderLayout.CENTER);
                    mainPanel.repaint();
                    slideLabel.setText((current + 1) + "/" + notifications.size());
                }

            }
        });
        postponeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ebus.post(new PostPoneTaskEvent(0, 5, notifications.get(current).getTask()));
            }
        });

        setVisible(true);

    }
}
