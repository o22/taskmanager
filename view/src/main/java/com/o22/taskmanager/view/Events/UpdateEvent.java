package com.o22.taskmanager.view.events;

import com.o22.taskmanager.model.Task;

import java.util.Set;

/**
 * Created by Александр on 08.12.2015.
 */
public class UpdateEvent extends ClientEvent {
    Set<Task> tasks;
    public UpdateEvent(Set<Task> tasks){
        this.tasks=tasks;

    }
    public Set<Task> getTasks(){
        return tasks;
    }
}
