package com.o22.taskmanager.view.events;

import com.o22.taskmanager.model.Task;

/**
 * Created by Александр on 09.12.2015.
 */
public class ShowEditViewEvent extends ClientEvent {
    private Task task=null;

    public ShowEditViewEvent(Task task) {

        this.task = task;
    }

    public Task getTask() {
        return task;
    }
}
