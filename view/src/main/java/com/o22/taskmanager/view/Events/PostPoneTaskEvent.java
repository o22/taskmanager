package com.o22.taskmanager.view.events;

import com.o22.taskmanager.model.Task;

/**
 * Created by Александр on 13.12.2015.
 */
public class PostPoneTaskEvent {
   private int min,hour;
    private Task task;

    public PostPoneTaskEvent(int hour, int min,Task task) {
        this.hour = hour;
        this.min = min;
        this.task=task;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }
    public Task getTask(){
        return task;
    }
}
