package com.o22.taskmanager.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwingConfirmBox extends JDialog {

    private boolean answer;

    public void display(String title, String message) {

        setModal(true);

        setTitle(title);
        setLocationRelativeTo(null);
        setSize(300, 130);
        setResizable(false);

        JPanel panel = new JPanel(new BorderLayout(10, 10));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JLabel messageLabel = new JLabel();
        messageLabel.setText(message);
        messageLabel.setHorizontalAlignment(SwingConstants.CENTER);

        JButton yesButton = new JButton();
        yesButton.setText("YES");
        yesButton.addActionListener(new YesButtonActionListener());

        JButton noButton = new JButton();
        noButton.setText("NO");
        noButton.addActionListener(new NoButtonActionListener());

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(yesButton);
        buttonPanel.add(noButton);

        panel.add(messageLabel, BorderLayout.CENTER);
        panel.add(buttonPanel, BorderLayout.SOUTH);
        add(panel);

        setVisible(true);
    }


    public boolean getAnswer() {
        return answer;
    }

    private class YesButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            answer = true;
            setVisible(false);
            dispose();
        }
    }

    private class NoButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            answer = false;
            setVisible(false);
            dispose();
        }
    }
}
