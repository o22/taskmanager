package com.o22.taskmanager.view.notification.factory;

import com.o22.taskmanager.controller.INotification;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.notification.swing.SwingNotification;

public class SwingINotificationFactory implements INotificationFactory {

    public INotification createNotification(Task task) {
        return new SwingNotification(task,null);
    }
}
