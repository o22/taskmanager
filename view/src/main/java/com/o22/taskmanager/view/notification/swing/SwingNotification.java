package com.o22.taskmanager.view.notification.swing;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.o22.taskmanager.controller.INotification;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.events.CompleteTaskEvent;
import com.o22.taskmanager.view.events.EventNotification;
import com.o22.taskmanager.view.events.PostPoneTaskEvent;
import com.o22.taskmanager.view.components.NotificationPanel;
import org.springframework.context.annotation.Scope;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.GregorianCalendar;

@org.springframework.stereotype.Component
@Scope("prototype")
public class SwingNotification extends JDialog implements INotification {

    private int answer = COMPLETE;
    private Task task;
    private JSpinner hourPicker;
    private JSpinner minPicker;
    private int postHour=0,postMin=0;
    private EventBus ebus;

    public SwingNotification(EventBus ebus) {
        this.ebus=ebus;
    }

    public SwingNotification(Task task,EventBus ebus) {
        this.task = task;
        this.ebus = ebus;
    }
    @Subscribe
    public void onStart(EventNotification event){
        System.out.println("EventNotification applied");
        this.task=event.getTask();
        display();
    }

    public void display() {
        Calendar c = new GregorianCalendar();
        setModal(true);

        setTitle("Task: " + task.getName());
        setLocationRelativeTo(null);
        setSize(500, 400);

        JPanel mainPanel = new JPanel(new BorderLayout(10, 10));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        NotificationPanel notificationPanel = new NotificationPanel(task);

        SpinnerModel hoursModel =
                new SpinnerNumberModel(0,
                        0, //min
                        24, //max
                        1);//step
        SpinnerModel minModel =
                new SpinnerNumberModel(5,
                        0, //min
                        59, //max
                        1);//step

        hourPicker = new JSpinner(hoursModel);

        minPicker = new JSpinner(minModel);
        JPanel timePanel = new JPanel(new FlowLayout());
        timePanel.add(hourPicker);
        timePanel.add(minPicker);
        JLabel onLabel = new JLabel("on");
        JLabel hourLabel = new JLabel("h");
        JLabel minLabel = new JLabel("m");

        JButton postponeButton = new JButton();
        postponeButton.setText("Postpone task");
        postponeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                postHour=Integer.parseInt(hourPicker.getValue().toString());
                postMin=Integer.parseInt(minPicker.getValue().toString());
                ebus.post(new PostPoneTaskEvent(postHour,postMin,task));
                //answer = POSTPONE;

                close();
            }
        });

        final JButton completeButton = new JButton();
        completeButton.setText("Complete task");
        completeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ebus.post(new CompleteTaskEvent(task));
                close();
            }
        });




        JPanel buttonsPanel = new JPanel(new FlowLayout());
        buttonsPanel.add(completeButton);

        buttonsPanel.add(postponeButton);
        buttonsPanel.add(onLabel);
        buttonsPanel.add(hourPicker);
        buttonsPanel.add(hourLabel);
        buttonsPanel.add(minPicker);

        buttonsPanel.add(minLabel);

        mainPanel.add(notificationPanel, BorderLayout.CENTER);
       // mainPanel.add(timePanel, BorderLayout.WEST);
        mainPanel.add(buttonsPanel, BorderLayout.SOUTH);

        add(mainPanel);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                completeButton.doClick();
            }
        });

        setVisible(true);
    }
    public int getTimeHour(){
        return postHour;
    }
    public int getTimeMin(){
        return postMin;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public int getAnswer() {
        return answer;
    }

    private void close() {
        //mainView updateUI ?
        setVisible(false);
        dispose();
    }

}
