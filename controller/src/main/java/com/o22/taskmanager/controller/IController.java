package com.o22.taskmanager.controller;

import com.o22.taskmanager.model.Task;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Set;

/**
 * Created by Александр on 25.10.2015.
 */
public interface IController {

    public void addTask(Task task);

    public void addTask(String name, Calendar date, String contacts, String description);

    public void deleteTask(Task task);

    public Set<Task> getJournal();

    public Set<Task> getMissedTasks();

    public void addTask(String name, String description, LocalDate ld, LocalTime lt, String contacts);
}
