package com.o22.taskmanager.controller;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;


import com.o22.taskmanager.model.IJournal;
import com.o22.taskmanager.model.Journal;
import com.o22.taskmanager.model.Task;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class Controller implements IController, Observable {

    private List<Observer> observers;

    private static final Logger log = Logger.getLogger(Controller.class);
    private Set<Task> missedTasks;
    private IJournal journal;

    private ILoaderSaver ls;
    /*@Autowired
    private ApplicationContext appContext;*/

    @Autowired
    public void setLs(ILoaderSaver ls) {
        log.debug("Autowired ls...");
        this.ls = ls;
    }

    @Autowired
    public void setjournal(IJournal journal) {
        log.debug("Autowired journal...");
        this.journal = journal;

    }


    public Controller() {
        //journal.setJournal(ls.loadJournal());
        missedTasks = new HashSet<Task>();
        log.debug("Create bean controller");
        observers = new ArrayList<Observer>();
        //startTimers();
    }

    public void addTask(Task task){
        log.debug("Adding new task...");
        journal.addTask(task);
        save();
        log.info("New task added and saved");
        startTimer(task);
    }
    
    public void addTask(String name, Calendar date, String contacts, String description) {
        log.debug("Adding new task...");
        Task t = new Task(name, description, date, contacts);
        journal.addTask(t);
        save();
        log.info("New task added and saved");
        startTimer(t);
        //log.debug("Start timer for new task");

    }

    public void addTask(String name, String description, LocalDate ld, LocalTime lt, String contacts) {
        log.debug("Adding new task...");
        LocalDate locd = ld;
        LocalTime loct = lt;
        Calendar calendar = new GregorianCalendar(ld.getYear(), ld.getMonthValue(), ld.getDayOfMonth(), lt.getHour(), lt.getMinute());
        Task task = new Task(name, description, calendar, contacts);
        journal.addTask(task);
        save();
        log.info("New task added and saved");
        startTimer(task);
        //log.debug("Start timer for new task");
    }

    public void deleteTask(Task task) {
        log.debug("Deleting task...");
        boolean c = journal.deleteTask(task);
        if (!c){
            log.info("Deleting error");
        }
        else {
            log.info("Task deleted");
            save();
        }
    }

    public void save() {
        log.debug("Saving journal...");
        ls.saveJournal(journal.getTasks());
        log.info("Journal was saved!");
        notifyObservers();
    }

    public void startTimer(Task t) {

        log.debug("Starting timers...");
        /*try {
            TimerTask task = new TaskTimer(t, this, (INotification) appContext.getBean("notif"));
            Timer timer = new Timer();
            Date today = Calendar.getInstance().getTime();
            timer.schedule(task, t.getDate().getTime().getTime() - today.getTime());
            log.debug("Start timer for task " + t.getName());
        } catch (IllegalArgumentException e) {
            log.debug("Task " + t.getName() + " are outdated", e);
            missedTasks.add(t);
            //deleteTask(t);
        }*/

        log.info("All timers was started");
    }

    public Set<Task> getMissedTasks() {
        log.debug("Getting missed tasks");
       // getJournal();//If journal==null, init the journal;
        return missedTasks;
    }

    public Set<Task> getJournal() {
        if (journal.getTasks() == null) {
            log.debug("Loading journal from save file...");
            journal.setTasks(ls.loadJournal());
            log.info("Load journal complete!");
            /*for (Task task : journal.getTasks()) {
                startTimer(task);
            }*/
            for(Task task : missedTasks){
                journal.deleteTask(task);
            }
        }

        ls.saveJournal(journal.getTasks());
        return journal.getTasks();
    }

    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(this);
        }
    }
}
