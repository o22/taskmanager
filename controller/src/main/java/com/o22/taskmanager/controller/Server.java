package com.o22.taskmanager.controller;

import com.o22.taskmanager.model.Constants;
import com.o22.taskmanager.model.Task;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Component;
import sun.rmi.runtime.Log;

import javax.swing.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * Created by Nastya on 06.12.2015.
 */
@Component
public class Server {

    private static final Logger LOG = Logger.getLogger(Server.class);
    private static Controller controller = (Controller) new GenericXmlApplicationContext(("app-context1.xml")).getBean("controller");

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        ServerSocket serverSocket = new ServerSocket(Constants.PORT);
        Task task;
        String check = "";
        while (true) {
            try {
                LOG.info("Waiting for a client...");
                Socket socket = serverSocket.accept();
                LOG.info("Got a client");
                DataInputStream in = new DataInputStream(socket.getInputStream());
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                ObjectInputStream inO = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream outO = new ObjectOutputStream(socket.getOutputStream());
                while (true){
                    check = in.readUTF();
                    LOG.info("Got from client: " + check);
                    if ((Constants.GET_JOURNAL).equals(check)) {
                        Set<Task> journal = controller.getJournal();
                        outO.reset();
                        outO.writeObject(journal);
                        outO.flush();
                    }
                    if ((Constants.ADD_TASK).equals(check)) {
                        task = (Task) inO.readObject();
                        controller.addTask(task);
                    }
                    if ((Constants.DELETE_TASK).equals(check)) {
                        task = (Task) inO.readObject();
                        controller.deleteTask(task);
                    }
                    if ((Constants.EXIT).equals(check)) {
                        LOG.info("Client is gone");
                        break;
                    }
                }
            }
            catch (EOFException e){
                LOG.info("EOFException");
            }
        }
    }
}