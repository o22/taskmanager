package com.o22.taskmanager.controller;


import com.o22.taskmanager.model.Task;

import java.util.Set;

/**
 * Created by Александр on 01.11.2015.
 */
public interface ILoaderSaver {

    void saveJournal(Set<Task> tasks);

    Set<Task> loadJournal();
}
