package com.o22.taskmanager.controller;

import com.o22.taskmanager.model.Task;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimerTask;

/**
 * Created by Александр on 15.11.2015.
 */
public class TaskTimer extends TimerTask {

    private static final Logger log = Logger.getLogger(TaskTimer.class);
    private INotification alertBox;
    Controller c;
    Task task;

    TaskTimer(Task t, Controller c, INotification alertBox) {
        task = t;
        this.c = c;

        this.alertBox = alertBox;
        alertBox.setTask(task);

    }

    @Override
    public void run() {
        if(c.getJournal().contains(task)) {


            alertBox.display();
            if (alertBox.getAnswer() == INotification.COMPLETE)
                c.deleteTask(task);
            if (alertBox.getAnswer() == INotification.POSTPONE) {
                Calendar today = new GregorianCalendar();
                task.setOrgDate(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), today.get(Calendar.HOUR), today.get(Calendar.MINUTE) + 5,0);
                c.save();
            }
        }
        //c.deleteTask(task);
    }


}