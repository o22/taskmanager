package com.o22.taskmanager.controller;

import com.o22.taskmanager.model.Journal;
import com.o22.taskmanager.model.Task;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.xml.bind.*;
import java.io.*;
import java.util.HashSet;
import java.util.Set;

@Component
public class LoaderSaverXML implements ILoaderSaver {

    private static final Logger LOG = Logger.getLogger(LoaderSaverXML.class);

    public Set<Task> loadJournal() {
        Set<Task> tasks = null;
        Journal journal = new Journal();
        File newFile = new File("save.xml");
        try {
            if(newFile.createNewFile()){
                tasks = new HashSet<Task>();
                LOG.info("File not found. Creating new file");
            }
            else{
                JAXBContext jaxbContext = JAXBContext.newInstance(Journal.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                journal = (Journal) unmarshaller.unmarshal(newFile);
                if (journal.getTasks()==null){
                    newFile.delete();
                    newFile.createNewFile();
                    tasks = new HashSet<Task>();
                    LOG.info("File already exist, but it's empty, load..");
                }
                else {
                    tasks = journal.getTasks();
                    LOG.info("File already exist, load from file");
                }
            }
        } catch (IOException e) {
            LOG.error("IOException", e);
        } catch (JAXBException e) {
            LOG.error("JAXBException by saveJournal", e);
        }
        return tasks;
    }

    public void saveJournal(Set<Task> tasks) {
        Journal journal = new Journal();
        journal.setTasks(tasks);
        try {
            File newFile = new File("save.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Journal.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(journal, newFile);
        } catch (PropertyException e) {
            LOG.error("PropertyException by saveJournal", e);
        } catch (JAXBException e) {
            LOG.error("JAXBException by saveJournal", e);
        }
    }
}