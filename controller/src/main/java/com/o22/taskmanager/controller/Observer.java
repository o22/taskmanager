package com.o22.taskmanager.controller;

public interface Observer {

    void update(Object obj);
}
