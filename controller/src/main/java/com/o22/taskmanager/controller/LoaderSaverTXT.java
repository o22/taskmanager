package com.o22.taskmanager.controller;

import com.o22.taskmanager.model.Task;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

@Component
public class LoaderSaverTXT {//implements ILoaderSaver {
    
    private static final Logger LOG = Logger.getLogger(LoaderSaverTXT.class);

    public Set<Task> loadJournal() {
        Set<Task> tasks = null;
        File newFile = new File("save.tm");
        try {
            if (newFile.createNewFile()) {
                tasks = new HashSet<Task>();
                LOG.info("File not found. Creating new file");
            } else {
                FileInputStream fis = new FileInputStream("save.tm");
                ObjectInputStream oin = new ObjectInputStream(fis);
                tasks = (HashSet<Task>) oin.readObject();
                LOG.info("File already exist, load from file");
            }
        } catch (IOException e) {
            LOG.error("IOException", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Class not found", e);
        }
        return tasks;
    }

    public void saveJournal(Set<Task> tasks) {
        try {
            FileOutputStream fos = new FileOutputStream("save.tm");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(tasks);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            LOG.error("IOException by saveJournal", e);
        }
    }
}