package com.o22.taskmanager.controller;

import com.o22.taskmanager.model.Task;

public interface INotification {

    int POSTPONE = 2;
    int COMPLETE = 3;

    void display();
    public void setTask(Task task);

    int getAnswer();
    int getTimeMin();
    int getTimeHour();
}
