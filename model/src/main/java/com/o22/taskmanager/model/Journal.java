package com.o22.taskmanager.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Александр on 25.10.2015.
 */
@Component
@XmlRootElement
public class Journal implements IJournal {

    private Set<Task> tasks;

    public Journal() {

    }

    public void addTask(Task task) {
        if (tasks == null)
            tasks = new HashSet<Task>();
        tasks.add(task);
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public boolean deleteTask(Task task) {
        return tasks.remove(task);
    }
    
    @XmlElement
    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Journal journal = (Journal) o;

        if (tasks != null ? !tasks.equals(journal.tasks) : journal.tasks != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return tasks != null ? tasks.hashCode() : 0;
    }
}
