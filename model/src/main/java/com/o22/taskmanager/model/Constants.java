package com.o22.taskmanager.model;

/**
 * Created by Александр on 19.12.2015.
 */

public class Constants {
    public static final String GET_JOURNAL="getJournal";
    public static final String ADD_TASK="addTask";
    public static final String DELETE_TASK="deleteTask";
    public static final String EXIT="exit";
    public static final String LOCALHOST = "127.0.0.1";
    public static final int PORT = 5555;
}
