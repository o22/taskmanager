package com.o22.taskmanager.model;

import java.util.Set;

/**
 * Created by Nastya on 01.11.2015.
 */
public interface IJournal {

    void addTask(Task task);

    Set<Task> getTasks();

    boolean deleteTask(Task task);

    void setTasks(Set<Task> tasks);
}